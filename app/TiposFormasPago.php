<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposFormasPago extends Model
{
    protected $table = 'tipo_formas_pagos';
}
