<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\FormasPago;
use Response;
use Validator;
class FormasPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(FormasPago::all(), 200);
    }

    public function formasPagoByUsuario($id)
    {
        $objectSee = FormasPago::whereRaw('usuario=?',$id)->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'numeros'          => 'required',
            'usuario'          => 'required',
            'tipo'          => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new FormasPago();
                $newObject->titulo            = $request->get('titulo','direccion');
                $newObject->numero            = $request->get('numero');
                $newObject->descripcion            = $request->get('descripcion');
                $newObject->observacion            = $request->get('observacion');
                $newObject->usuario            = $request->get('usuario');
                $newObject->tipo            = $request->get('tipo');
                $newObject->estado            = $request->get('estado');
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = FormasPago::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = FormasPago::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->titulo            = $request->get('titulo',$objectUpdate->titulo);
                $objectUpdate->numero            = $request->get('numero',$objectUpdate->numero);
                $objectUpdate->descripcion       = $request->get('descripcion',$objectUpdate->descripcion);
                $objectUpdate->observacion       = $request->get('observacion',$objectUpdate->observacion);
                $objectUpdate->usuario           = $request->get('usuario',$objectUpdate->usuario);
                $objectUpdate->tipo              = $request->get('tipo',$objectUpdate->tipo);
                $objectUpdate->estado            = $request->get('estado',$objectUpdate->estado);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = FormasPago::find($id);
        if ($objectDelete) {
            try {
                FormasPago::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
