<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
use App\Ventas;
use App\VentasDetalle;
use App\Inventario;
use App\CuentasCobrar;
use Response;
use DB;
use Validator;

class VentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Ventas::where('estado','=','1')->with('clientes','tipos')->get(), 200);
    }

    public function anuladas()
    {
        return Response::json(Ventas::where('estado','=','0')->with('clientes','tipos')->get(), 200);
    }

    public function comprobante()
    {
        $objectSee = Ventas::orderby('id','desc')->first();
        if ($objectSee) {
            $objectSee->clientes;
            return Response::json($objectSee, 200);
        
        }
        else {
            $myObject = (object) array("comprobante" => 0);
            return Response::json($myObject, 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cliente'        => 'required',
            'usuario'        => 'required',
            'total'          => 'required',
            'fecha'          => 'required',
            'tipo'           => 'required',
            'detalle'        => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                DB::beginTransaction();
                $newObject = new Ventas();
                $newObject->cliente              = $request->get('cliente');
                $newObject->usuario              = $request->get('usuario');
                $newObject->total                = $request->get('total');
                $newObject->fecha                = $request->get('fecha');
                $newObject->tipo                 = $request->get('tipo');
                $newObject->direccion            = $request->get('direccion');
                $newObject->formapago            = $request->get('formapago');
                $newObject->comprobante          = $request->get('comprobante');
                $newObject->unit_price           = $request->get('unit_price');
                $newObject->quantity             = $request->get('quantity');
                $newObject->ern                  = $request->get('ern');
                $newObject->token                = $request->get('token');
                $newObject->aprobacion           = $request->get('aprobacion');
                $newObject->fechaapro            = $request->get('fechaapro');
                $newObject->save();
                if($newObject->tipo==2 || $newObject->tipo=='2'){
                    $newCount = new CuentasCobrar();
                    $newCount->creditoDado            = $request->get('total');
                    $newCount->total                  = $request->get('total');
                    $newCount->plazo                  = $request->get('plazo');
                    $newCount->tipoPlazo              = $request->get('tipoPlazo');
                    $newCount->venta                  = $newObject->id;
                    $newCount->save();
                }
                if ( $request->get('detalle') )
                {
                   $Array = $request->get('detalle');
                   foreach ($Array as $value)
                    {
                        $objectUpdate = Inventario::find($value['id']);
                        if ($objectUpdate) {
                            try {
                                    $objectUpdate->cantidad           = (($objectUpdate->cantidad-$value['cantidad']));
                                    $objectUpdate->save();
                                    $registro = new VentasDetalle();
                                    $registro->subtotal    = $value['subtotal'];
                                    $registro->cantidad    = $value['cantidad'];
                                    $registro->precio      = $value['precioVenta'];
                                    $registro->precioE     = $value['precioClienteEs'];
                                    $registro->precioM     = $value['precioDistribuidor'];
                                    $registro->venta       = $newObject->id;
                                    $registro->producto    = $value['producto'];
                                    $registro->tipo        = $newObject->tipo;
                                    $registro->save();
                            } catch (Exception $e) {
                                $returnData = array (
                                    'status' => 500,
                                    'message' => $e->getMessage()
                                );
                                return Response::json($returnData, 500);
                            }
                        }
                        else {
                            $returnData = array (
                                'status' => 404,
                                'message' => 'No record found'
                            );
                            return Response::json($returnData, 404);
                        }
                    }
                    
                    DB::commit();
                    // $returnData = array (
                    //     'status' => 200,
                    //     'message' => "success"
                    // );
                    // return Response::json($returnData, 200);
                }
                else
                {
                    DB::rollback();
                    $returnData = array (
                        'status' => 400,
                        'message' => 'Invalid Parameters'
                    );
                    return Response::json($returnData, 400);
                }
                $newObject->detalle;
                
                return Response::json($newObject, 200);
            
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Ventas::with('detalle','clientes','tipos')->find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }


    public function findErnOrders($ern){
        $objectSee = Ventas::with('detalle','clientes','tipos')->whereRaw('ern=?',$ern)->first();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function ventasByClient($id)
    {
        $objectSee = Ventas::whereRaw('cliente=?',$id)->with('detalle','clientes','tipos')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function estadisticaVendedoresBarra(Request $request){
        $objectSee = \DB::table('ventas')
        ->select(DB::raw('sum(ventas.total) as total'),'usuarios.username','ventas.fecha')
        ->join('usuarios', 'usuarios.id', '=', 'ventas.usuario')
        ->whereRaw('(ventas.fecha>=? and ventas.fecha<=?) and ventas.estado=1',[$request->get('fechaInicio'),$request->get('fechaFin')])
        ->groupBy('usuarios.username')
        ->groupBy(DB::raw('date(ventas.fecha)'))
        ->get();
        
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function estadisticaClientesBarra(Request $request){
        $objectSee = \DB::table('ventas')
        ->select(DB::raw('sum(ventas.total) as total'),'clientes.nombre','clientes.apellido','ventas.fecha')
        ->join('clientes', 'clientes.id', '=', 'ventas.cliente')
        ->whereRaw('(ventas.fecha>=? and ventas.fecha<=?) and ventas.estado=1',[$request->get('fechaInicio'),$request->get('fechaFin')])
        ->groupBy('clientes.nombre','clientes.apellido')
        ->groupBy(DB::raw('date(ventas.fecha)'))
        ->get();
        
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function estadisticaVentasBarra(Request $request){
        $objectSee = \DB::table('ventas')
        ->select(DB::raw('sum(ventas.total) as total'),'ventas.fecha')
        // ->join('clientes', 'clientes.id', '=', 'ventas.cliente')
        ->whereRaw('(ventas.fecha>=? and ventas.fecha<=?) and ventas.estado=1',[$request->get('fechaInicio'),$request->get('fechaFin')])
        // ->groupBy('clientes.nombre','clientes.apellido')
        ->groupBy(DB::raw('date(ventas.fecha)'))
        ->get();
        
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function estadisticaVentasPie(Request $request){
        $objectSee = \DB::table('ventas')
        ->select(DB::raw('sum(ventasdetalle.cantidad) as total'),'ventasdetalle.subtotal','ventas.fecha','productos.nombre')
        ->join('ventasdetalle', 'ventasdetalle.venta', '=', 'ventas.id')
        ->join('productos', 'productos.id', '=', 'ventasdetalle.producto')
        ->join('usuarios', 'usuarios.id', '=', 'ventas.usuario')
        ->whereRaw('(ventas.fecha>=? and ventas.fecha<=?) and ventas.estado=1',[$request->get('fechaInicio'),$request->get('fechaFin')])
        ->groupBy('productos.id')
        ->orderby(DB::raw('sum(ventasdetalle.cantidad)'),'desc')
        ->limit(5)
        ->get();
        //$sql = "SELECT (dv.subtotal),sum(dv.cantidad),p.nombre,p.codigoproducto,p.tiporepuesto FROM ventas v  
        // inner join ventasdetalle dv on dv.idventa=v.idventas 
        // inner join productos p on p.idproductos=dv.idproductos 
        // inner join usuarios u on v.idusuario=u.idusuarios 
        // where (v.fecha>'".$datos[0]."' and v.fecha<'".$fecha3."') and v.estado=1 
        // group by p.idproductos order by sum(dv.cantidad) desc limit 5;";
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function estadisticaClientesPie(Request $request){
        $objectSee = \DB::table('ventas')
        ->select(DB::raw('sum(ventas.total) as total'),'clientes.nombre','clientes.apellido','ventas.fecha')
        ->join('clientes', 'clientes.id', '=', 'ventas.cliente')
        ->whereRaw('(ventas.fecha>=? and ventas.fecha<=?) and ventas.estado=1',[$request->get('fechaInicio'),$request->get('fechaFin')])
        ->groupBy('clientes.nombre','clientes.nombre')
        ->get();
        
        if ($objectSee) {

            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function estadisticaVendedoresPie(Request $request){
        $objectSee = \DB::table('ventas')
        ->select(DB::raw('sum(ventas.total) as total'),'usuarios.username','ventas.fecha')
        ->join('usuarios', 'usuarios.id', '=', 'ventas.usuario')
        ->whereRaw('(ventas.fecha>=? and ventas.fecha<=?) and ventas.estado=1',[$request->get('fechaInicio'),$request->get('fechaFin')])
        ->groupBy('usuarios.username')
        ->get();
        
        if ($objectSee) {

            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $objectUpdate = Ventas::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->estado = $request->get('estado', $objectUpdate->estado);
                if($objectUpdate->tipo==2 || $objectUpdate->tipo=='2'){
                    $objectDelete = CuentasCobrar::whereRaw('venta=?',[$objectUpdate->id])->first();
                    if ($objectDelete) {
                        try {
                            CuentasCobrar::destroy($objectDelete->id);
                        } catch (Exception $e) {
                            DB::rollback();
                            $returnData = array (
                                'status' => 500,
                                'message' => $e->getMessage()
                            );
                            return Response::json($returnData, 500);
                        }
                    }
                    else {
                        DB::rollback();
                        $returnData = array (
                            'status' => 404,
                            'message' => 'No record found'
                        );
                        return Response::json($returnData, 404);
                    }
                }
                $objectUpdate->save();
                $objectUpdate2 = VentasDetalle::whereRaw('venta=?',$id)->get();
                if ($objectUpdate2) {
                    try {
                        foreach ($objectUpdate2 as $value) {
                            $actualiza = VentasDetalle::find($value['id']);
                            if ($actualiza) {
                                try {
                                    $actualiza->estado = $request->get('estado', $actualiza->estado);
                                        $inventario = Inventario::whereRaw('producto=?',[$actualiza->producto])->first();
                                        if ($inventario) {
                                            $inventario->cantidad = $inventario->cantidad+$actualiza->cantidad;
                                            $inventario->save();
                                        }
                                    $actualiza->save();
                                } catch (Exception $e) {
                                    DB::rollback();
                                    $returnData = array (
                                        'status' => 500,
                                        'message' => $e->getMessage()
                                    );
                                    return Response::json($returnData, 500);
                                }
                            }
                            else {
                                DB::rollback();
                                $returnData = array (
                                    'status' => 404,
                                    'message' => 'No record found'
                                );
                                return Response::json($returnData, 404);
                            }
                        }
                        DB::commit();
                        return Response::json($objectUpdate, 200);
                    } catch (Exception $e) {
                        DB::rollback();
                        $returnData = array (
                            'status' => 500,
                            'message' => $e->getMessage()
                        );
                        return Response::json($returnData, 500);
                    }
                }
                else {
                    DB::rollback();
                    $returnData = array (
                        'status' => 404,
                        'message' => 'No record found'
                    );
                    return Response::json($returnData, 404);
                }
            } catch (Exception $e) {
                DB::rollback();
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            DB::rollback();
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function pagar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cantidad'          => 'required',
            'precio'            => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'No ha llenado los campos adecuadamente.',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            /*
             * Lo primero es crear el objeto nusoap_client, al que se le pasa como
             * parámetro la URL de Conexión definida en la constante WSPG
             */
            define("UID", "00a44a36adaab6310e6bf306b9d5969b");
            define("WSK", "c6ef3680408284ca1addc87b64c48421");
            define("SANDBOX", true);
            $Pagadito = new Pagadito(UID, WSK);
            /*
             * Si se está realizando pruebas, necesita conectarse con Pagadito SandBox. Para ello llamamos
             * a la función mode_sandbox_on(). De lo contrario omitir la siguiente linea.
             */
            if (SANDBOX) {
                $Pagadito->mode_sandbox_on();
            }
            /*
             * Validamos la conexión llamando a la función connect(). Retorna
             * true si la conexión es exitosa. De lo contrario retorna false
             */
            if ($Pagadito->connect()) {
                /*
                 * Luego pasamos a agregar los detalles
                 */
                if ($request->get("cantidad") > 0) {
                    $Pagadito->add_detail($request->get("cantidad"), $request->get("descripcion"), $request->get("precio"), $request->get("url"));
                }
                
                
                //Agregando campos personalizados de la transacción
                $Pagadito->set_custom_param("param1", "Valor de param1");
                $Pagadito->set_custom_param("param2", "Valor de param2");
                $Pagadito->set_custom_param("param3", "Valor de param3");
                $Pagadito->set_custom_param("param4", "Valor de param4");
                $Pagadito->set_custom_param("param5", "Valor de param5");
        
                //Habilita la recepción de pagos preautorizados para la orden de cobro.
                $Pagadito->enable_pending_payments();
        
                /*
                 * Lo siguiente es ejecutar la transacción, enviandole el ern.
                 *
                 * A manera de ejemplo el ern es generado como un número
                 * aleatorio entre 1000 y 2000. Lo ideal es que sea una
                 * referencia almacenada por el Pagadito Comercio.
                 */
                if($request->get("ern"))
                {
                    $ern = $request->get("ern");
                }
                else{
                    $ern = rand(1000, 2000);
                }
                $eeror = $Pagadito->exec_trans($ern);
                if (!$eeror) {
                    /*
                     * En caso de fallar la transacción, verificamos el error devuelto.
                     * Debido a que la API nos puede devolver diversos mensajes de
                     * respuesta, validamos el tipo de mensaje que nos devuelve.
                     */
                    switch($Pagadito->get_rs_code())
                    {
                        case "PG2001":
                            {
                                $returnData = array (
                                    'status' => 400,
                                    'message' => 'Data incompleta'
                                );
                                return Response::json($returnData, 400);
                                break;
                            /*Incomplete data*/
                        }
                        case "PG3002":
                            {
                                $returnData = array (
                                    'status' => 400,
                                    'message' => 'Error General'
                                );
                                return Response::json($returnData, 400);
                                break;
                        }
                            /*Error*/
                        case "PG3003":
                            {
                                $returnData = array (
                                    'status' => 400,
                                    'message' => 'transaccion sin registrar'
                                );
                                return Response::json($returnData, 400);
                                break;
                        }
                            /*Unregistered transaction*/
                        case "PG3004":
                            {
                                $returnData = array (
                                    'status' => 400,
                                    'message' => 'Error matematico'
                                );
                                return Response::json($returnData, 400);
                                break;
                        }
                            /*Match error*/
                        case "PG3005":
                            {
                                $returnData = array (
                                    'status' => 400,
                                    'message' => 'Error de conexion'
                                );
                                return Response::json($returnData, 400);
                                break;
                        }
                            /*Disabled connection*/
                        default:
                            $returnData = array (
                                'status' => 400,
                                'message' => 'No ha podido funcionar 1 '.$Pagadito->get_rs_code()
                            );
                            return Response::json($returnData, 400);
                            break;
                    }
                }else{
                    $returnData = array (
                        'status' => 200,
                        'message' => 'Funcionando genial',
                        'token' => $eeror
                    );
                    return Response::json($returnData, 200);
                }
            } else {
                /*
                 * En caso de fallar la conexión, verificamos el error devuelto.
                 * Debido a que la API nos puede devolver diversos mensajes de
                 * respuesta, validamos el tipo de mensaje que nos devuelve.
                 */
                switch($Pagadito->get_rs_code())
                {
                    case "PG2001":
                        /*Incomplete data*/
                    case "PG3001":
                        /*Problem connection*/
                    case "PG3002":
                        /*Error*/
                    case "PG3003":
                        /*Unregistered transaction*/
                    case "PG3005":
                        /*Disabled connection*/
                    case "PG3006":
                        /*Exceeded*/
                    default:
                        $returnData = array (
                            'status' => 400,
                            'message' => 'No ha podido funcionar 2'
                        );
                        return Response::json($returnData, 400);
                        break;
                }
            }
        
        }
    }public function comprobanteCompra($id,Request $request)
    {
        if ($request->get('token')) {
            /*
             * Lo primero es crear el objeto Pagadito, al que se le pasa como
             * parámetros el UID y el WSK definidos en config.php
             */
            
            define("UID", "00a44a36adaab6310e6bf306b9d5969b");
            define("WSK", "c6ef3680408284ca1addc87b64c48421");
            define("SANDBOX", true);
            $Pagadito = new Pagadito(UID, WSK);
            /*
             * Si se está realizando pruebas, necesita conectarse con Pagadito SandBox. Para ello llamamos
             * a la función mode_sandbox_on(). De lo contrario omitir la siguiente linea.
             */
            if (SANDBOX) {
                $Pagadito->mode_sandbox_on();
            }
            /*
             * Validamos la conexión llamando a la función connect(). Retorna
             * true si la conexión es exitosa. De lo contrario retorna false
             */
            if ($Pagadito->connect()) {
                /*
                 * Solicitamos el estado de la transacción llamando a la función
                 * get_status(). Le pasamos como parámetro el token recibido vía
                 * GET en nuestra URL de retorno.
                 */
                if ($Pagadito->get_status($request->get('token'))) {

                    /*
                     * Luego validamos el estado de la transacción, consultando el
                     * estado devuelto por la API.
                     */
                    switch($Pagadito->get_rs_status())
                    {
                        case "COMPLETED":{
                            /*
                             * Tratamiento para una transacción exitosa.
                             */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                             $objectUpdate = Ventas::whereRaw('id',$id)->with('clientes','usuarios','detalle')->first();
                                if ($objectUpdate) {
                                    if(!$objectUpdate->token && !$objectUpdate->aprobacion && !$objectUpdate->fechaapro){
                                        $returnData11 = array (
                                            'status' => 200,
                                            'token' => $request->get('token'),
                                            'aprobacion' => $Pagadito->get_rs_reference(),
                                            'fecha' => $Pagadito->get_rs_date_trans(),
                                            'message' => 'Compra Exitosa'
                                        );
                                        try {
                                            if($objectUpdate->token==NULL){
                                                $objectUpdate->token = $request->get('token', $objectUpdate->token);
                                                $objectUpdate->aprobacion = $Pagadito->get_rs_reference();
                                                $objectUpdate->fechaapro = $Pagadito->get_rs_date_trans();
                                                $objectUpdate->estado              = 1;
                                                $objectUpdate->save();
                                                if ($objectUpdate->usuarios && $objectUpdate->clientes) {
                                                    
                                                    Mail::send('emails.pago', ['empresa' => 'GTechnology', 'url' => 'http://gtechnology.gt', 'app' => 'http://me.gtechnology.gt', 'autorizacion' => $Pagadito->get_rs_reference(), 'fecha' => $Pagadito->get_rs_date_trans(), 'username' => $objectUpdate->usuarios->username, 'email' => $objectUpdate->usuarios->email, 'name' => $objectUpdate->clientes->nombre.' '.$objectUpdate->clientes->apellido,], function (Message $message) use ($objectUpdate){
                                                        $message->from('info@foxylabs.gt', 'Info GTechnology')
                                                                ->sender('info@foxylabs.gt', 'Info GTechnology')
                                                                ->to($objectUpdate->usuarios->email, $objectUpdate->clientes->nombre.' '.$objectUpdate->clientes->apellido)
                                                                ->replyTo('info@foxylabs.gt', 'Info GTechnology')
                                                                ->subject('Comprobante de Pago');
                                                    });
                                                
                                                }
                                                else {
                                                    $returnData = array (
                                                        'status' => 404,
                                                        'message' => 'No record found'
                                                    );
                                                    return Response::json($returnData, 404);
                                                }
                                            
                                            }
                                        } catch (Exception $e) {
                                            $returnData = array (
                                                'status' => 500,
                                                'message' => $e->getMessage()
                                            );
                                            return Response::json($returnData, 500);
                                        }
                                        return Response::json($returnData11, 200);
                                    }else{
                                        $returnData11 = array (
                                            'status' => 201,
                                            'token' => $objectUpdate->token,
                                            'aprobacion' => $objectUpdate->aprobacion,
                                            'fecha' => $objectUpdate->fechaapro,
                                            'message' => 'Compra Exitosa'
                                        );
                                        return Response::json($returnData11, 201);
                                    }
                                    
                                }
                                else {
                                    $returnData = array (
                                        'status' => 404,
                                        'message' => 'No record found'
                                    );
                                    return Response::json($returnData, 404);
                                }
                            }
                        
                        case "REGISTERED":{
                            
                            /*
                             * Tratamiento para una transacción aún en
                             * proceso.
                             */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                             $returnData = array (
                                'status' => 400,
                                'token' => $request->get('token'),
                                'message' => "operacion Cancelada"
                            );
                            return Response::json($returnData, 400);
                            break;
                        }
                        
                        case "VERIFYING":{
                            
                            /*
                             * La transacción ha sido procesada en Pagadito, pero ha quedado en verificación.
                             * En este punto el cobro xha quedado en validación administrativa.
                             * Posteriormente, la transacción puede marcarse como válida o denegada;
                             * por lo que se debe monitorear mediante esta función hasta que su estado cambie a COMPLETED o REVOKED.
                             */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            $returnData = array (
                                 'status' => 200,
                                 'token' => $request->get('token'),
                                 'aprobacion' => $Pagadito->get_rs_reference(),
                                 'fecha' => $Pagadito->get_rs_date_trans(),
                                 'message' => 'Compra en Validacion'
                             );
                             return Response::json($returnData, 200);
                            break;}
                        
                        case "REVOKED":{
                            
                            /*
                             * La transacción en estado VERIFYING ha sido denegada por Pagadito.
                             * En este punto el cobro ya ha sido cancelado.
                             */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                             $returnData = array (
                                'status' => 400,
                                'token' => $request->get('token'),
                                'message' => "Compra Denegada"
                            );
                            return Response::json($returnData, 400);
                            break;}
                        
                        case "FAILED":
                            /*
                             * Tratamiento para una transacción fallida.
                             */
                            {
                                $returnData = array (
                                    'status' => 500,
                                    'token' => $request->get('token'),
                                    'message' => "Compra Denegada"
                                );
                                return Response::json($returnData, 500);
                            }
                        default:
                            {
                            /*
                             * Por ser un ejemplo, se muestra un mensaje
                             * de error fijo.
                             */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                             $returnData = array (
                                'status' => 500,
                                'token' => $request->get('token'),
                                'message' => "Compra Denegada"
                            );
                            return Response::json($returnData, 500);
                            break;}
                    }
                } else {
                    /*
                     * En caso de fallar la petición, verificamos el error devuelto.
                     * Debido a que la API nos puede devolver diversos mensajes de
                     * respuesta, validamos el tipo de mensaje que nos devuelve.
                     */
                    switch($Pagadito->get_rs_code())
                    {
                        case "PG2001":
                            /*Incomplete data*/
                        case "PG3002":
                            /*Error*/
                        case "PG3003":
                            /*Unregistered transaction*/
                        default:
                            /*
                             * Por ser un ejemplo, se muestra un mensaje
                             * de error fijo.
                             */ ///////////////////////////////////////////////////////////////////////////////////////////////////////
                             $returnData = array (
                                'status' => 500,
                                'token' => $request->get('token'),
                                'message' => "error de transaccion Denegada"
                            );
                            return Response::json($returnData, 500);
                            break;
                    }
                }
            } else {
                /*
                 * En caso de fallar la conexión, verificamos el error devuelto.
                 * Debido a que la API nos puede devolver diversos mensajes de
                 * respuesta, validamos el tipo de mensaje que nos devuelve.
                 */
                switch($Pagadito->get_rs_code())
                {
                    case "PG2001":
                        /*Incomplete data*/
                    case "PG3001":
                        /*Problem connection*/
                    case "PG3002":
                        /*Error*/
                    case "PG3003":
                        /*Unregistered transaction*/
                    case "PG3005":
                        /*Disabled connection*/
                    case "PG3006":
                        /*Exceeded*/
                    default:
                        /*
                         * Aqui se muestra el código y mensaje de la respuesta del WSPG
                         */
                        $returnData = array (
                            'status' => 400,
                            'token' => $request->get('token'),
                            'message' => "Compra Denegada",
                            'COD' => $Pagadito->get_rs_code(),
                            'MSG' => $Pagadito->get_rs_message()
                        );
                        return Response::json($returnData, 400);
                        
                        break;
                }
            }
        } else {
            /*
             * Aqui se muestra el mensaje de error al no haber recibido el token por medio de la URL.
             */
            $returnData = array (
                'status' => 400,
                'token' => $request->get('token'),
                'message' => "no se recibieron los datos",
            );
            return Response::json($returnData, 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Ventas::find($id);
        if ($objectDelete) {
            try {
                Ventas::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}