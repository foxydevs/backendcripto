<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposDirecciones extends Model
{
    protected $table = 'tipo_direcciones';
}
