<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDireccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direcciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->nullable()->default(null);
            $table->string('direccion')->nullable()->default(null);
            $table->string('observacion')->nullable()->default(null);
            $table->tinyInteger('estado')->nullable()->default(1);
            
            $table->integer('usuario')->unsigned()->nullable()->default(null);
            $table->foreign('usuario')->references('id')->on('usuarios')->onDelete('cascade');

            $table->integer('tipo')->unsigned()->nullable()->default(null);
            $table->foreign('tipo')->references('id')->on('tipo_direcciones')->onDelete('cascade');

            $table->integer('departamento')->unsigned()->nullable()->default(null);
            $table->foreign('departamento')->references('id')->on('departamentos')->onDelete('cascade');
            $table->integer('municipio')->unsigned()->nullable()->default(null);
            $table->foreign('municipio')->references('id')->on('municipios')->onDelete('cascade');
            $table->integer('pais')->unsigned()->nullable()->default(null);
            $table->foreign('pais')->references('id')->on('paises')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direcciones');
    }
}
