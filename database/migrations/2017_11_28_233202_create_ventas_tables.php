<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->double('total',50,2)->nullable()->default(null);
            $table->date('fecha')->nullable()->default(null);
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->double('unit_price')->nullable()->default(null);
            $table->double('quantity')->nullable()->default(null);
            $table->timestamp('created')->useCurrent();
            $table->string('ern')->nullable()->default(null);
            $table->string('token')->nullable()->default(null);
            $table->string('aprobacion')->nullable()->default(null);
            $table->string('fechaapro')->nullable()->default(null);
            $table->string('comprobante')->nullable()->default(null);

            $table->integer('tipo')->unsigned()->nullable()->default(null);
            $table->foreign('tipo')->references('id')->on('tiposventa')->onDelete('cascade');

            $table->integer('direccion')->unsigned()->nullable()->default(null);
            $table->foreign('direccion')->references('id')->on('direcciones')->onDelete('cascade');

            $table->integer('formapago')->unsigned()->nullable()->default(null);
            $table->foreign('formapago')->references('id')->on('formas_pagos')->onDelete('cascade');

            $table->integer('cliente')->unsigned()->nullable()->default(null);
            $table->foreign('cliente')->references('id')->on('clientes')->onDelete('cascade');

            $table->integer('usuario')->unsigned()->nullable()->default(null);
            $table->foreign('usuario')->references('id')->on('usuarios')->onDelete('cascade');

            $table->integer('sucursal')->unsigned()->nullable()->default(null);
            $table->foreign('sucursal')->references('id')->on('sucursales')->onDelete('cascade');
            
            $table->integer('creador')->unsigned()->nullable()->default(null);
            $table->foreign('creador')->references('id')->on('usuarios')->onDelete('cascade');
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
