<?php

use Illuminate\Database\Seeder;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'id'               => 1,
            'username'         => "admin",
            'password'         => bcrypt('foxylabs'),
            'email'            => "admin@foxylabs.gt",
            'privileges'       => 1,
            'rol'              => 2,
            'empleado'         => null,
            'sucursal'         => null,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_formas_pagos')->insert([
            'descripcion'      => "Tarjeta",
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_formas_pagos')->insert([
            'descripcion'      => "Pagadito",
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_formas_pagos')->insert([
            'descripcion'      => "Efectivo",
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_formas_pagos')->insert([
            'descripcion'      => "Criptomoneda",
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_direcciones')->insert([
            'descripcion'      => "Principal",
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_direcciones')->insert([
            'descripcion'      => "Secundaria",
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_direcciones')->insert([
            'descripcion'      => "Oficina",
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('tipo_direcciones')->insert([
            'descripcion'      => "Casa",
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('direcciones')->insert([
            'titulo'           => "Casa Guate",
            'direccion'        => "5ta Av 4-29 zona 2",
            'observacion'      => "a la vuelta de parque morazan",
            'usuario'          => 1,
            'tipo'             => 4,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('direcciones')->insert([
            'titulo'           => "Casa MAzate",
            'direccion'        => "3ra Av 7-52 Zona 1",
            'observacion'      => "Media Cuadra antes de la liga del corazon",
            'usuario'          => 1,
            'tipo'             => 3,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('formas_pagos')->insert([
            'titulo'           => "Tarjeta Bac",
            'numero'           => "0000 0000 0000",
            'descripcion'      => "Limite de tanto",
            'observacion'      => "",
            'usuario'          => 1,
            'tipo'             => 1,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('empleados')->insert([
            'nombre'           => "Daniel",
            'apellido'         => "Rodriguez",
            'direccion'        => "Guatemala",
            'telefono'         => "54646431",
            'celular'          => "54646431",
            'sueldo'           => 7000,
            'estado'           => 1,
            'puesto'           => 2,
            'sucursal'         => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('usuarios')->insert([
            'id'               => 2,
            'username'         => "daniel",
            'password'         => bcrypt('foxylabs'),
            'email'            => "daniel@foxylabs.gt",
            'privileges'       => 1,
            'rol'              => 1,
            'empleado'         => 1,
            'sucursal'         => null,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('direcciones')->insert([
            'titulo'           => "Casa Guate",
            'direccion'        => "5ta Av 4-29 zona 2",
            'observacion'      => "a la vuelta de parque morazan",
            'usuario'          => 2,
            'tipo'             => 4,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('direcciones')->insert([
            'titulo'           => "Casa MAzate",
            'direccion'        => "3ra Av 7-52 Zona 1",
            'observacion'      => "Media Cuadra antes de la liga del corazon",
            'usuario'          => 2,
            'tipo'             => 3,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('formas_pagos')->insert([
            'titulo'           => "Tarjeta Bi",
            'numero'           => "0000 0000 0001",
            'descripcion'      => "Limite tanto",
            'observacion'      => "",
            'usuario'          => 2,
            'tipo'             => 1,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        
        DB::table('empleados')->insert([
            'nombre'           => "Alejandro",
            'apellido'         => "Godoy",
            'direccion'        => "Guatemala",
            'telefono'         => "54795431",
            'celular'          => "54685431",
            'sueldo'           => 17000,
            'estado'           => 1,
            'puesto'           => 1,
            'sucursal'         => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('usuarios')->insert([
            'id'               => 3,
            'username'         => "alejandro",
            'password'         => bcrypt('foxylabs'),
            'email'            => "alejandro@foxylabs.gt",
            'privileges'       => 1,
            'rol'              => 3,
            'empleado'         => 2,
            'sucursal'         => null,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('proveedores')->insert([
            'nombre'           => "CF",
            'direccion'        => "Ciudad",
            'telefono'         => "000000",
            'cuenta'           => "000000",
            'nit'              => "CF",
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('clientes')->insert([
            'nombre'           => "CF",
            'apellido'         => "CF",
            'direccion'        => "Ciudad",
            'telefono'         => "000000",
            'celular'           => "000000",
            'nit'              => "CF",
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('clientes')->insert([
            'nombre'           => "Daniel Rodriguez",
            'apellido'         => "Rodriguez",
            'direccion'        => "Ciudad",
            'telefono'         => "54646431",
            'celular'           => "000000",
            'nit'              => "8191874-7",
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('usuarios')->insert([
            'username'         => "cliente",
            'password'         => bcrypt('foxylabs'),
            'email'            => "cliente@foxylabs.gt",
            'privileges'       => 1,
            'rol'              => 1,
            'cliente'          => 2,
            'sucursal'         => null,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('direcciones')->insert([
            'titulo'           => "Casa Guate",
            'direccion'        => "5ta Av 4-29 zona 2",
            'observacion'      => "a la vuelta de parque morazan",
            'usuario'          => 4,
            'tipo'             => 4,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('direcciones')->insert([
            'titulo'           => "Casa MAzate",
            'direccion'        => "3ra Av 7-52 Zona 1",
            'observacion'      => "Media Cuadra antes de la liga del corazon",
            'usuario'          => 4,
            'tipo'             => 3,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('formas_pagos')->insert([
            'titulo'           => "Tarjeta Bi",
            'numero'           => "0000 0000 0001",
            'descripcion'      => "Limite tanto",
            'observacion'      => "",
            'usuario'          => 4,
            'tipo'             => 1,
            'estado'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
    }
}
