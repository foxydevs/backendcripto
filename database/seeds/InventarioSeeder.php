<?php

use Illuminate\Database\Seeder;

class InventarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inventario')->insert([
            'precioCosto'           => "8.50",
            'precioVenta'           => "15.00",
            'precioClienteEs'       => "18.50",
            'precioDistribuidor'    => "12",
            'cantidad'              => 3,
            'minimo'                => 1,
            'descuento'             => 0,
            'estado'                => 1,
            'creador'               => 1,
            'sucursal'              => 1,
            'producto'              => 1,
            'deleted_at'            => null,
            'created_at'            => date('Y-m-d H:m:s'),
            'updated_at'            => date('Y-m-d H:m:s')
        ]);
        DB::table('inventario')->insert([
            'precioCosto'           => "10.00",
            'precioVenta'           => "18.00",
            'precioClienteEs'       => "20.50",
            'precioDistribuidor'    => "15.50",
            'cantidad'              => 3,
            'minimo'                => 1,
            'descuento'             => 0,
            'estado'                => 1,
            'creador'               => 1,
            'sucursal'              => 1,
            'producto'              => 2,
            'deleted_at'            => null,
            'created_at'            => date('Y-m-d H:m:s'),
            'updated_at'            => date('Y-m-d H:m:s')
        ]);
        DB::table('inventario')->insert([
            'precioCosto'           => "50.00",
            'precioVenta'           => "80.00",
            'precioClienteEs'       => "85.00",
            'precioDistribuidor'    => "75.50",
            'cantidad'              => 3,
            'minimo'                => 1,
            'descuento'             => 0,
            'estado'                => 1,
            'creador'               => 1,
            'sucursal'              => 1,
            'producto'              => 3,
            'deleted_at'            => null,
            'created_at'            => date('Y-m-d H:m:s'),
            'updated_at'            => date('Y-m-d H:m:s')
        ]);
    }
}
