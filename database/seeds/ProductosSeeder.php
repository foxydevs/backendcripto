<?php

use Illuminate\Database\Seeder;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([
            'descripcion'      => "Producto 1",
            'nombre'           => "2 Ruedas",
            'codigo'           => 1,
            'marcaDes'         => "Rebaja",
            'estado'           => 1,
            'tipo'             => 1,
            'marca'            => 1,
            'creador'          => 4,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('productos')->insert([
            'descripcion'      => "Producto 2",
            'nombre'           => "4 Ruedas",
            'codigo'           => 1,
            'marcaDes'         => "Economico",
            'estado'           => 1,
            'tipo'             => 1,
            'marca'            => 2,
            'creador'          => 4,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('productos')->insert([
            'descripcion'      => "Producto 1",
            'nombre'           => "2 Ruedas",
            'codigo'           => 1,
            'marcaDes'         => "Rebajada",
            'estado'           => 1,
            'tipo'             => 1,
            'marca'            => 3,
            'creador'          => 4,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
    }
}
