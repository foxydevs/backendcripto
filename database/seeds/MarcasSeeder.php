<?php

use Illuminate\Database\Seeder;

class MarcasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marcas')->insert([
            'nombre'           => "Susuki",
            'descripcion'      => "De Motocicleta",
            'estado'           => 1,
            'creador'          => 4,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('marcas')->insert([
            'nombre'           => "Kia",
            'descripcion'      => "De Carro",
            'estado'           => 1,
            'creador'          => 4,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('marcas')->insert([
            'nombre'           => "Honda",
            'descripcion'      => "De Motocicleta",
            'estado'           => 1,
            'creador'          => 4,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
    }
}
